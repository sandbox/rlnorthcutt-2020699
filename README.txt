-- SUMMARY --

This module provides a simplified JS interface for adding, editing, and
reordering webform components from the view page.

-- REQUIREMENTS --

* Webform
* jQuery plugin - BlockUI

-- INSTALLATION --

* Enable module as usual.
* Download http://malsup.github.com/jquery.blockUI.js and put it in
    sites/all/libraries/blockui/

-- CONFIGURATION --

* Go to /admin/config/content/webform/easy_edit

-- TROUBLESHOOTING --


-- CONTACT --
Current maintainers:
* Patrick O'Brian (Patrick OBrian) - http://drupal.org/user/767682
* Ron Northcutt (rlnorthcutt) - http://drupal.org/user/23506
